import json
import sqlite3
import datetime
import os.path


def create_tables():

    """ Функція створює таблиці AllSales і DaySales. """

    # Список із назвами стовпців для таблиці 'AllSales'.
    all_sales_cols = [
        'Date',
        'WeekDay',
        'ChequeId',
        'ChequeProductsQuantity',
        'ShopName',
        'ShopId',
        'ChequeTotalPrice',
    ]

    # Створення таблиці 'AllSales'.
    db_cursor.execute(
        "CREATE TABLE IF NOT EXISTS "
        """AllSales(
            {0} TEXT,
            {1} INTEGER,
            {2} INTEGER,
            {3} FLOAT,
            {4} TEXT,
            {5} INTEGER,
            {6} FLOAT,
            PRIMARY KEY ({0}, {1}, {2}, {5})
        );""".format(*all_sales_cols)
    )

    # Список із назвами стовпців для таблиці 'DaySales'.
    day_sales_cols = [
        'ChequeId',
        'ShopId',
        'Date',
        'WeekDay',
        'ChequePositionNumber',
        'ProductPrice',
        'ProductName',
        'ProductID',
        'ProductQuantity',
        'ProductTotalPrice',
    ]

    # Створення таблиці 'DaySales'.
    db_cursor.execute(
        "CREATE TABLE IF NOT EXISTS "
        """DaySales(
            {0} INTEGER,
            {1} INTEGER,
            {2} TEXT,
            {3} INTEGER,
            {4} INTEGER,
            {5} FLOAT,
            {6} TEXT,
            {7} INTEGER,
            {8} FLOAT,
            {9} FLOAT,
            FOREIGN KEY ({0}, {1}, {2}, {3}) REFERENCES
                AllSales(ChequeId, ShopId, Date, WeekDay)
        );""".format(*day_sales_cols)
    )


def fill_all_sales():

    """ Функція наповнює таблицю 'AllSales'. """

    print('Завантаження даних до таблиці "AllSales"...')

    try:

        # Підготовка запису бази даних для таблиці AllSales.
        for ind in range(len(json_data) + 1):
            insertion_data = [

                # Перетворюєм дату в читабельний формат
                datetime.datetime.strptime(
                    json_data[ind]['date'], '%Y-%m-%dT%H:%M:%S.%fZ'
                ).strftime('%d.%m.%Y, %H:%M:%S'),

                json_data[ind]['week_day'],
                json_data[ind]['id'],
                json_data[ind]['items_qty'],
                json_data[ind]['shop__name'],
                json_data[ind]['shop_id'],
                json_data[ind]['total_price'],
            ]

            db_cursor.execute(
                "INSERT INTO AllSales VALUES (?, ?, ?, ?, ?, ?, ?)",
                insertion_data
            )

    # Обробка виключень, повідомлення користувача про помилку та/або
    # завершення наповнення таблиці.
    except IndexError:
        db_connect.commit()
        print('Таблицю "AllSales" заповнено.')

    except sqlite3.IntegrityError:
        print('Помилка заповнення таблиці "AllSales".\n')

    finally:
        input('\nНатисніть "Enter" для продовження...\n')


def fill_day_sales():

    """ Функція наповнює таблицю 'DaySales'."""
    # Допускається можливість повторного запису тих самих даних в таблицю.

    print('Завантаження даних до таблиці "DaySales"...')

    try:

        for ind in range(len(json_data) + 1):

            # Підготовка запису бази даних для таблиці DaySales.
            for prod_ind in range(len(json_data[ind]['products'])):

                insertion_data = [
                    json_data[ind]['id'],
                    json_data[ind]['shop_id'],

                    # Перетворюєм дату в читабельний формат
                    datetime.datetime.strptime(
                        json_data[ind]['date'], '%Y-%m-%dT%H:%M:%S.%fZ'
                    ).strftime('%d.%m.%Y, %H:%M:%S'),

                    json_data[ind]['week_day'],
                    json_data[ind]['products'][prod_ind]['order_no'],
                    json_data[ind]['products'][prod_ind]['price'],
                    json_data[ind]['products'][prod_ind]['product__name'],
                    json_data[ind]['products'][prod_ind]['product_id'],
                    json_data[ind]['products'][prod_ind]['qty'],
                    json_data[ind]['products'][prod_ind]['total_price'],
                ]

                db_cursor.execute(
                    "INSERT INTO DaySales "
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", insertion_data
                )

    # Обробка виключень, повідомлення користувача про помилку та/або
    # завершення наповнення таблиці.
    except IndexError:
        db_connect.commit()
        print('Таблицю "DaySales" заповнено.')

    except sqlite3.IntegrityError:
        print('Помилка заповнення таблиці "DaySales"\n')

    finally:
        input('\nНатисніть "Enter" для продовження...\n')


def run():
    try:
        create_tables()
        fill_all_sales()
        fill_day_sales()
    except KeyboardInterrupt:
        print('\nЗупинено користувачем.')
    finally:
        db_cursor.close()
        db_connect.close()
        print('Роботу завершено.')


# Перевіряємо чи існує база даних та файл data.json.
if not os.path.isfile('sales_data.db'):

    if os.path.isfile('data.json'):
        # Завантаження даних із data.json для їх обробки.
        json_data = json.loads(open('data.json').read())

        # Підключення бази даних.
        db_connect = sqlite3.connect('sales_data.db')
        db_cursor = db_connect.cursor()
        db_cursor.execute("PRAGMA FOREIGN_KEYS=ON;")

        run()

    else:
        print('Файл "data.json" не знайдено.\n')
        input('Натисніть "Enter" для виходу...')

else:
    print('База даних вже існує.\n')
    input('Натисніть "Enter" для виходу...')
