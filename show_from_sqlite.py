import sqlite3
import datetime

db_connect = sqlite3.connect('sales_data.db')
db_cursor = db_connect.cursor()

# ОСНОВНІ ФУНКЦІЇ ПРОГРАМИ


# Топ 10 по обороту, кількості чеків, кількості продажів.
def top_ten_by_turnover():

    """ Функція повертає топ 10 товарів по обороту у вигляді списка. """

    db_cursor.execute(
        "SELECT ProductName, SUM(ProductTotalPrice) as PrcSUM FROM DaySales "
        "GROUP BY ProductName ORDER BY -PrcSUM LIMIT 10")
    output = db_cursor.fetchall()

    return output


def top_ten_by_cheque_qty():

    """ Функція повертає топ 10 товарів
    по кількості чеків у вигляді списка. """

    db_cursor.execute(
        "SELECT ProductName, COUNT(ChequeId) as ChqIdCnt FROM DaySales "
        "GROUP BY ProductName ORDER BY -ChqIdCnt LIMIT 10")
    output = db_cursor.fetchall()

    return output


def top_ten_by_sales_qty():

    """ Функція повертає топ 10 товарів
     по кількості продажів у вигляді списка. """

    db_cursor.execute(
        "SELECT ProductName, SUM(ProductQuantity) as ProdQtySUM FROM DaySales "
        "GROUP BY ProductName ORDER BY -ProdQtySUM LIMIT 10")
    output = db_cursor.fetchall()

    return output


# Кількість чеків магазину "назва магазину".
def cheques_quantity_by_shop(shop_id):

    """ Функція повертає кількість чеків по вказаному магазину. """

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM AllSales WHERE ShopId={0}"
        .format(shop_id)
    )

    return db_cursor.fetchone()[0]


# Кількість чеків товару "назва товару".
def cheques_quantity_by_product(product_id):

    """ Функція повертає кількість чеків по вказаному товару. """

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM DaySales WHERE ProductID={0}"
        .format(product_id)
    )

    return db_cursor.fetchone()[0]


# Середній чек товару "назва товару".
def average_product_cheque(product_id):

    """ Функція повертає середній чек по вказаному товару. """

    db_cursor.execute(
        "SELECT AVG(ProductTotalPrice) FROM DaySales WHERE ProductID={0}"
        .format(product_id)
    )

    return db_cursor.fetchone()[0]


# Оборот, кількість чеків, кількість продажів по магазинах.
def turnover_by_shops():

    """ Функція повертає оборот по магазинам у вигляді словника. """

    output = {'Shop №01': None, 'Shop №02': None, 'Shop №03': None}

    db_cursor.execute(
        "SELECT "
        "SUM(ChequeTotalPrice)"
        "FROM AllSales WHERE ShopId=641"
    )
    output['Shop №01'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT "
        "SUM(ChequeTotalPrice) "
        "FROM AllSales WHERE ShopId=595"
    )
    output['Shop №02'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT "
        "SUM(ChequeTotalPrice) "
        "FROM AllSales WHERE ShopId=601"
    )
    output['Shop №03'] = int(db_cursor.fetchone()[0])

    return output


def cheques_qty_by_shops():

    """ Функція повертає кількість чеків по магазинам у вигляді словника. """

    output = {'Shop №01': None, 'Shop №02': None, 'Shop №03': None}

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM AllSales WHERE ShopId=641"
    )
    output['Shop №01'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM AllSales WHERE ShopId=595"
    )
    output['Shop №02'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM AllSales WHERE ShopId=601"
    )
    output['Shop №03'] = int(db_cursor.fetchone()[0])

    return output


def sales_qty_by_shops():

    """ Функція повертає кількість продажів
     по магазинам у вигляді словника. """

    output = {'Shop №01': None, 'Shop №02': None, 'Shop №03': None}

    db_cursor.execute(
        "SELECT SUM(ChequeProductsQuantity) FROM AllSales WHERE ShopId=641"
    )
    output['Shop №01'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT SUM(ChequeProductsQuantity) FROM AllSales WHERE ShopId=595"
    )
    output['Shop №02'] = int(db_cursor.fetchone()[0])

    db_cursor.execute(
        "SELECT SUM(ChequeProductsQuantity) FROM AllSales WHERE ShopId=601"
    )
    output['Shop №03'] = int(db_cursor.fetchone()[0])

    return output


# Кількість чеків товару "назва товару" по днях тижня.
def cheque_qty_by_week_day(weekday, product_id):

    """ Функція повертає кількість чеків по вказаному товару та дню тижня. """

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM DaySales WHERE "
        "WeekDay={0} AND ProductID={1}".format(int(weekday), str(product_id))
    )

    return db_cursor.fetchone()[0]


# Кількість чеків товару "назва товару" по днях.
def cheque_qty_by_date(date, product_no):

    """ Функція повертає кількість чеків по вказаному товару та дню (даті). """

    db_cursor.execute(
        "SELECT COUNT(ChequeId) FROM DaySales "
        "WHERE Date LIKE '%{0}%' AND ProductId={1}".format(date, product_no)
    )

    return db_cursor.fetchone()[0]


# ЗАПУСК ПРОГРАМИ, РОБОТА З ОСНОВНИМИ ФУНКЦІЯМИ


def run():
    try:

        # Меню:
        choice = int(input(
            """ Оберіть запит:
            1. Топ 10 товарів по обороту/ кількості чеків/ кількості продажів.
            2. Кількість чеків магазину.
            3. Кількість чеків товару.
            4. Середній чек товару.
            5. Оборот/ кількість чеків/ кількість продажів по магазинах.
            6. Кількість чеків товару по днях тижня.
            7. Кількість чеків товару по днях (даті).
            8. Вихід.
\nЗапит: """
        ))

        # Топ 10 товарів по обороту/ кількості чеків/ кількості продажів.
        if choice == 1:

            choice_1 = int(input(
                """ Оберіть запит:
                1. Топ 10 товарів по обороту.
                2. Топ 10 товарів по кількості чеків.
                3. Топ 10 товарів по кількості продажів.
\nЗапит: """
            ))

            # Топ 10 товарів по обороту.
            if choice_1 == 1:
                print("\nТоп 10 товарів по обороту:\n")
                for product in top_ten_by_turnover():
                    print(
                        '{0:<45} {1}'.format(product[0][:40], int(product[1]))
                    )

            # Топ 10 товарів по кількості чеків.
            elif choice_1 == 2:
                print("\nТоп 10 товарів по кількості чеків:\n")
                for product in top_ten_by_cheque_qty():
                    print(
                        '{0:<45} {1}'.format(product[0][:40], int(product[1]))
                    )

            # Топ 10 товарів по кількості продажів.
            elif choice_1 == 3:
                print("\nТоп 10 товарів по кількості продажів:\n")
                for product in top_ten_by_sales_qty():
                    print(
                        '{0:<45} {1}'.format(product[0][:40], int(product[1]))
                    )

            else:
                raise ValueError

            continue_prompt()

        # Кількість чеків по магазину.
        elif choice == 2:

            choice_2 = int(input(""" Оберіть запит:
                1. Магазин "Shop №01".
                2. Магазин "Shop №02".
                3. Магазин "Shop №03".
\nЗапит: """))

            # Кількість чеків по магазину Shop №01.
            if choice_2 == 1:
                print(
                    'Кількість чеків магазину Shop №01: {0}'
                    .format(cheques_quantity_by_shop(641))
                )

            # Кількість чеків по магазину Shop №01.
            elif choice_2 == 2:
                print(
                    'Кількість чеків магазину Shop №02: {0}'
                    .format(cheques_quantity_by_shop(595))
                )

            # Кількість чеків по магазину Shop №01.
            elif choice_2 == 3:
                print(
                    'Кількість чеків магазину Shop №03: {0}'
                    .format(cheques_quantity_by_shop(601))
                )

            else:
                raise ValueError

            continue_prompt()

        # Кількість чеків товару.
        elif choice == 3:

            # Запускаємо цикл while і будемо питати id,
            # доки користувач не введе існуючий.
            ask_id = True
            while ask_id:

                product_id = int(input('Введіть ID товару: '))

                # Якщо введено існуючий id - припиняємо цикл.
                if cheques_quantity_by_product(product_id):
                    ask_id = False

                    # Отримуємо назву товару із бази даних по id.
                    db_cursor.execute(
                        "SELECT ProductName FROM DaySales WHERE ProductID={0}"
                        .format(int(product_id)))
                    product_name = db_cursor.fetchone()[0]

                    # Виводимо результат.
                    print('Кількість чеків товару "{0}": {1}'.format(
                        product_name,
                        cheques_quantity_by_product(product_id)
                    ))

                    continue_prompt()

                else:
                    print('Товар не існує.\n')

        # Середній чек товару.
        elif choice == 4:

            # Запускаємо цикл while і будемо питати id,
            # доки не буде введено існуючий.
            ask_id = True
            while ask_id:

                product_id = int(input('Введіть ID товару: '))

                # Якщо введено існуючий id - припиняємо цикл.
                if average_product_cheque(product_id):
                    ask_id = False

                    # Отримуємо назву товару із бази даних по id, зберігаємо.
                    db_cursor.execute(
                        "SELECT ProductName FROM DaySales WHERE ProductID={0}"
                        .format(int(product_id)))
                    product_name = db_cursor.fetchone()[0]

                    # Виводимо результат.
                    print('Середній чек товару "{0}": {1}'
                          .format(product_name,
                                  round(average_product_cheque(product_id), 2))
                          )

                    continue_prompt()

                else:
                    print('Товар не існує.\n')

        # Оборот/ кількість чеків/ кількість продажів по магазинах.
        elif choice == 5:

            choice_5 = int(input(
                """ Оберіть запит:
                1. Оборот по магазинах.
                2. Кількість чеків по магазинах.
                3. Кількість продажів по магазинах.
\nЗапит: """
            ))

            # Оборот по магазинах.
            if choice_5 == 1:

                print(
                    'Оборот по магазину "Shop №01": {0}'
                    .format(turnover_by_shops()['Shop №01'])
                )

                print(
                    'Оборот по магазину "Shop №02": {0}'
                    .format(turnover_by_shops()['Shop №02'])
                )

                print(
                    'Оборот по магазину "Shop №03": {0}'
                    .format(turnover_by_shops()['Shop №03'])
                )

            # Кількість чеків по магазинах.
            elif choice_5 == 2:

                print(
                    'Кількість чеків магазину "Shop №01": {0}'
                    .format(cheques_qty_by_shops()['Shop №01'])
                )

                print(
                    'Кількість чеків магазину "Shop №02": {0}'
                    .format(cheques_qty_by_shops()['Shop №02'])
                )

                print(
                    'Кількість чеків магазину "Shop №03": {0}'
                    .format(cheques_qty_by_shops()['Shop №03'])
                )

            # Кількість продажів по магазинах.
            elif choice_5 == 3:

                print(
                    'Оборот по магазину "Shop №01": {0}'
                    .format(sales_qty_by_shops()['Shop №01'])
                )

                print(
                    'Оборот по магазину "Shop №02": {0}'
                    .format(sales_qty_by_shops()['Shop №02'])
                )

                print(
                    'Оборот по магазину "Shop №03": {0}'
                    .format(sales_qty_by_shops()['Shop №03'])
                )

            else:
                raise ValueError

            continue_prompt()

        # Кількість чеків товару по днях тижня.
        elif choice == 6:

            choice_6 = int(input(
                """ Оберіть запит:
                1. Понеділок.
                2. Вівторок.
                3. Середа.
                4. Четвер.
                5. П'ятниця.
                6. Суббота.
                7. Неділя.

\nЗапит: """
            ))

            # Понеділок.
            if choice_6 == 1:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))
                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару '
                            '"{0}" за понеділок: {1}\n'.format(
                                product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # Вівторок.
            if choice_6 == 2:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))
                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару "{0}" за вівторок: {1}\n'
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # Середа.
            if choice_6 == 3:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))

                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару "{0}" за середу: {1}\n'
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # Четвер.
            if choice_6 == 4:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))

                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару "{0}" за четвер: {1}\n'
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # П'ятниця.
            if choice_6 == 5:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))

                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            "\nКількість чеків товару '{0}' за п'ятницю: {1}\n"
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # Суббота.
            if choice_6 == 6:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))

                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару "{0}" за субботу: {1}\n'
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            # Неділя.
            if choice_6 == 7:

                # Запускаємо цикл while і будемо питати id,
                # доки не буде введено існуючий.
                ask_id = True
                while ask_id:

                    product_id = int(input('Введіть ID товару: '))

                    # Якщо введено існуючий id - припиняємо цикл.
                    if cheque_qty_by_week_day(choice_6 - 1, product_id):
                        ask_id = False

                        # Отримуємо назву товару із бази даних по id.
                        db_cursor.execute(
                            "SELECT ProductName FROM DaySales "
                            "WHERE ProductID={0}".format(int(product_id)))

                        product_name = db_cursor.fetchone()[0]

                        # Виводимо результат.
                        print(
                            '\nКількість чеків товару "{0}" за неділю: {1}\n'
                            .format(product_name, cheque_qty_by_week_day(
                                                                  choice_6 - 1,
                                                                  product_id))
                        )

                    else:
                        print('Товар не існує\n')

            else:
                run()

        # Кількість чеків товару по днях (даті).
        elif choice == 7:

            # Отримуємо від користувача дату і перевіряємо формат.
            date = input('Введіть дату (формат "дд.мм.рррр"): ')
            datetime.datetime.strptime(date, '%d.%m.%Y')

            product_id = int(input('Введіть ID товару: '))

            if cheque_qty_by_date(date, product_id):

                # Запит в базу даних.
                db_cursor.execute(
                    "SELECT ProductName FROM DaySales WHERE Date LIKE '%{0}%' "
                    "AND ProductID={1}"
                    .format(date, product_id)
                )
                product_name = db_cursor.fetchone()[0]

                # Виводимо результат на екран.
                print('\n Кількість чеків товару "{0}" станом на {1}: {2}.\n'
                      .format(product_name, date,
                              cheque_qty_by_date(date, product_id))
                      )

                continue_prompt()

            else:
                print('\nДаних за даним запитом не знайдено.\n')
                run()

        # Вихід.
        elif choice == 8:
            pass

    # Обробка виключень.
    except ValueError:
        print('\nПорушено формат запиту або його здійснення не можливе.\n')
        run()

    except KeyboardInterrupt:
        print('\nЗавершено користувачем.\n')


# "Продовжити роботу з програмою?"
def continue_prompt():

    choice = input("\nПродовжити роботу з програмою? (так/ні)\n>>>")
    if 'так' in choice.lower():
        run()
    else:
        pass

run()
